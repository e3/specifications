==============
specifications
==============

This repository contains e3 specifications. These are essentially definitions of releases for ESS' EPICS
environment. They describe a full environment, and are intended to act as a single source-of-truth to be
able to reproduce a given environment. The initial release definitions are described in closer detail on
ESS' intraweb (Confluence; `link`_).

These are designed for use with the tool `e3-build` from the package `e3`_;
for more information on usage, see that repository.

This repository is used together with gitlab CI to perform builds and deployments of EPICS modules.

.. _e3: https://gitlab.esss.lu.se/e3/e3
.. _link: https://confluence.esss.lu.se/x/g-vIFQ

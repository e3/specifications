# Changelog

All notable changes to this project will be documented in this file.

## [2022q1]

- Release 2022q1-core and 2022q1-full specifications
- Created repository and added various peripheral files.


[Unreleased]: https://gitlab.esss.lu.se/e3/e3/-/tree/master/
